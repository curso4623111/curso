import os #para limpar a tela temos q importar esse módulo
opc=0
banana=0
melancia=0
morango=0
cesta={}
while opc != 3:
    print(f"""
        *** Menu Principal ***
        1: Ver cesta
        2: Adicionar frutas
        3: Sair """)
    opc=(input ("Qual sua opção:"))

    if opc == "3":
        #limpa a tela
        if os.name == "nt":
            os.system("cls")
        else:
            os.system("clear")
        break
    elif opc == "2":
        print(f"""
        *** Menu Principal ***
        1: Banana $2.50
        2: Melancia $3.00
        3: Morango $2.00 """)
        opc2=(input ("Qual a fruta desejada:"))
        if opc2 == "1":
            cesta["bananas"] = 2.50
            banana=banana+1
        elif opc2=="2":
            cesta["melancias"] = 3.00
            melancia=melancia+1
        elif opc2=="3":
            cesta["morangos"] = 2.00
            morango=morango+1
        else:
            print ("*** Escolha Invalida ***")
    elif opc == "1":        
        print("*** Conteudo da Cesta ***")
        for x, y in cesta.items():  
            qtd=0
            if x=="bananas":
                qtd=banana
            elif x=="melancias":
                qtd=melancia
            else:
                qtd=morango
            vlr = qtd*y
            print(f"""
{qtd} {x} $ {vlr:.2f}
            """) #formata duas casas decimais
            
        # # outra maneira de usar o PRINT:
        # # uar o f antes das 3 aspas no inicio e fimal do comando
        # print(f""""
        #     *** Conteudo da Cesta ***
        #     Bananas - {banana}
        #     Melancias - {melancia}
        #     Morangos - {morango}""")
        #
        # print("*** Conteudo da Cesta ***")
        # print("Bananas - " ,banana)
        # print("Melancias - " , melancia)
        # print("Morangos - " ,morango)
    else:
        print("*** Opção Inválida ***")
        #limpa a tela
        if os.name == "nt":
            os.system("cls")
        else:
            os.system("clear")

